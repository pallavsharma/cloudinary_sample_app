require('dotenv').load();
const util = require('util');
const cloudinary = require('cloudinary').v2;


// File upload (example for promise api)
const upload = async (image_url, options = {}) => {
	console.log("...options", options)
    return await cloudinary.uploader.upload(image_url, { 
        ...options, 
        tags: 'basic_sample' 
    })
}


const transform_video = (video_public_id, transformation_attrs) => {
    return cloudinary.video(video_public_id, transformation_attrs)
}

module.exports = { upload, transform_video }
