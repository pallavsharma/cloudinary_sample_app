const { upload, transform_video } = require('./cloudinary')

// upload video
// upload image
// upload gif
// transform the video, add image and gif as an overlay

const get_result = async () => {
    let video = await upload('./assets/frame.mp4', { resource_type: "video" })
    let image = await upload('./assets/pizza.jpg')
    let gif = await upload('./assets/frame-gif.gif')
    let transform = transform_video(video.public_id, {
        transformation: [
	            // check for order
	            { overlay: image.public_id, width: 1080, crop: "scale"},
	            { overlay: gif.public_id, width: 1080, crop: "scale"}
        	]
    	}
	)

    console.log("video", video);
    console.log("image", image);
    console.log("gif", gif);
    console.log("transform", transform);
}

get_result()

// sample output
// https://res.cloudinary.com/www-pallavsharma/video/upload/c_scale,l_vvwwxjcxxnguls6wsuv4,w_1080/c_scale,l_wvvwlk9rs8wm1mqjsppw,w_1080/pyyo27b27hibskrteddi.mp4
// 
// 
