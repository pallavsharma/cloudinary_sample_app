# Cloudinary Node Sample Projects #

## Basic sample

The basic sample uploads local image to Cloudinary and generates URLs for applying various image transformations on the uploaded files.

### Setting up

1. Before running the sample, copy the Environment variable configuration parameters from Cloudinary's [Management Console](https://cloudinary.com/console) of your account into `.env` file of the project or export it (i.e. export CLOUDINARY_URL=xxx).
1. Run `npm install` in project directory to bring all the required modules. 
1. Run the sample using `node video_transform.js`.
